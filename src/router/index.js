import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from "../views/LoginView.vue";
import SignUpView from "../views/SignUpView.vue";
import SelfLearning from "../views/SelfLearningView.vue";
import ContactView from "../views/ContactView.vue";
import Admindashbord from "../views/AdminDashBordView.vue";
import StudentDashBord from "../views/StudentDashBord.vue";
import CourseView from "../views/CourseView.vue";
const routes = [
  {
    path: '/',
    name: 'home',
    alias:'/home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/AboutView.vue')
  },
  {
    path:'/login',
    name : 'login',
    component:LoginView
  },
  {
    path:'/signup',
    name:'signup',
    component:SignUpView
  },
  {
    path:'/selfLearning',
    name: 'selfLearning',
    component:SelfLearning
  },
  {
    path:'/contact',
    name: 'contact',
    component:ContactView
  },
  {
    path:'/admindashbord',
    name: 'admindashbord',
    component:Admindashbord
  },
  {
    path:'/studentdashbord',
    name: 'studentdashbord',
    component:StudentDashBord
  },
  {
    path:'/course',
    name: 'course',
    component:CourseView
  }

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
