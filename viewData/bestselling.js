export default [
    {
        tittle: "Java Script",
        image: "https://pluralsight2.imgix.net/paths/images/javascript-542e10ea6e.png",
        descr:"JavaScript is an interpreted programming language that conforms to the ECMAScript specification",
        course:"15" 
    },

    {
        tittle: "Angular",
        image: " https://pluralsight.imgix.net/paths/path-icons/angular-14a0f6532f.png",
        descr:"Angular is a complete JavaScript framework for creating dynamic and interactive applications in HTML.",
        course:"5" 
    },

    {
        tittle: "Core Phython",
        image: "https://pluralsight.imgix.net/paths/python-7be70baaac.png",
        descr:"Python is an interpreted, high-level, general-purpose programming language that emphasizes code readability.",
        course:"10" 
    }
    ,

    {
        tittle: "C#",
        image: "https://pluralsight.imgix.net/paths/path-icons/csharp-e7b8fcd4ce.png?",
        descr:"C# is the most commonly used language for leveraging the .NET Framework. As such, learning C# is a springboard to creating enterprise systems, desktop applications, websites and mobile applications. ",
        course:"12" 
    }
]