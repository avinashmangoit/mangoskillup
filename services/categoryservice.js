import http from '../http-common'

class category {
    getAllCategory() {
        return http.get('/All_category')
    }
    createCategory(categoryData){
        return http.post('/create_category_new',categoryData);
    }
    deleteCategory(categoryid){
        console.log(categoryid)
        return http.delete(`/Delete_cat/${categoryid}`)
    }
}
export default new category;