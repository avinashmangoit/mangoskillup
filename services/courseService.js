import http from '../http-common'

class course {
   getAllCategory() {
      return http.get('/All_category')


   }
   getTop3Course() {
      return http.get('/get_course');
   }


   createcourse(courseData, onUploadProgress) {
      let formData = new FormData();

      for (var key in courseData) {
         formData.append(key, courseData[key]);
      }
     return http.post("/create_course", formData, {
         headers: {
            "Content-Type": "multipart/form-data"
         },
         onUploadProgress
      });
   }


   deleteCourse(id){
      return http.delete(`/Delete_course/${id}`);
   }
   updateCourse(courseId , courseData,onUploadProgress){
      let formData = new FormData();

      for (var key in courseData) {
         formData.append(key, courseData[key]);
      }
     return  http.put(`/Update_course/${courseId}`, formData, {
         headers: {
            "Content-Type": "multipart/form-data"
         },
         onUploadProgress
      });
   }
}
export default new course