import http from '../http-common'

class user {
   
   signUp(userRegData){
       return http.post('/createUser',userRegData)
   }

    signIn(userData){
       return http.post('/login', userData)
    }
    getAllUser(){
        return http.get('/user_Detail')
     }
     updateUser(userid,userUpdateData){
        console.log(userid, userUpdateData)
        return http.put(`/user_update/${userid}`,userUpdateData,)
      }
     deleteUser(userid){
       return http.delete(`/user_Deleted/${userid}`)
     }


}
export default new user 